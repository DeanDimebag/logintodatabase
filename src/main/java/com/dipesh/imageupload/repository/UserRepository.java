package com.dipesh.imageupload.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.dipesh.imageupload.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {
	@Modifying 
	@Query(value = "insert into tbl_user (email, password) values (?,?)", nativeQuery = true)
	public int insertByEmailAndPassword (String email, String password);
}
