package com.dipesh.imageupload.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dipesh.imageupload.entity.DatabaseFile;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

@Repository
public interface DatabaseFileRepository extends JpaRepository<DatabaseFile, Integer> {

@Modifying    
//@Query(value = "INSERT INTO tbl_image (username, image_name) VALUES (?,?)", nativeQuery = true)
//public int insertByUserAndImage(String username, String image);

@Query(value = "INSERT INTO tbl_image (username, email, location, phone, image_name) VALUES (?,?,?,?,?)", nativeQuery = true)
public int insertByUserAndImage(String username, String email, String location, String phone, String image);
}
