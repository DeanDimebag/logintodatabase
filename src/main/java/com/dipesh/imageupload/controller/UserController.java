package com.dipesh.imageupload.controller;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dipesh.imageupload.entity.User;
import com.dipesh.imageupload.repository.UserRepository;

@Controller
@RequestMapping(value = "/user")
public class UserController {
	
	@Autowired
	private UserRepository repository;
	
	@GetMapping
	public String user(){
		return "/user/index";
	}
	
//	@PostMapping(value = "/save")
//	@Transactional
//	public String save(Model model, @RequestParam("email") String email, @RequestParam("password") String password){
//		repository.insertByEmailAndPassword(email, password);
//		model.addAttribute("msg", "User Detail Added Successfully");
//		return "user/uploadUser";
//	} 
	//FOR SAVING DATA INTO DATABASE
    @PostMapping
    @Transactional
    public String save(User model) {
        repository.save(model);
        return "redirect:/user?success";
    }
    //FOR SAVING DATA INTO JSON
    @PostMapping(value = "/json")
    @Transactional
    @ResponseBody
    public String saveJson(User model) {
        repository.save(model);
        return("success");
    }
    //FOR FETCHING ALL JSON DATA
    @GetMapping(value = "/json")
    @ResponseBody
    public List<User> json() {
        return repository.findAll();
    }
}
