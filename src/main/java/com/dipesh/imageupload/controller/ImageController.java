/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.imageupload.controller;

import com.dipesh.imageupload.entity.DatabaseFile;
import com.dipesh.imageupload.repository.DatabaseFileRepository;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Dipesh
 */
@Controller
@RequestMapping (value = "/image")
public class ImageController {
     @Autowired
    private DatabaseFileRepository repository;

    public static String uploadDirectory = System.getProperty("user.dir") + "/uploads";

    @GetMapping
    public String home() {
        return "index";
    }

  /*  @PostMapping(value = "/upload")
    @Transactional
    public String Upload(Model model, @RequestParam("username") String username, @RequestParam("imagename") MultipartFile imagename) {
        StringBuilder builder = new StringBuilder();
        Path fileNameAndPath = Paths.get(uploadDirectory, imagename.getOriginalFilename());
        builder.append(imagename.getOriginalFilename());
        try {
            Files.write(fileNameAndPath, imagename.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("msg", "Image Uploaded Successfully !");
        String image = imagename.getOriginalFilename().toString();
//        System.out.println("Image Name : " + image);
//        System.out.println(username);
        repository.insertByUserAndImage(username, image);
        return "image/uploadimage";
    }*/
    
  //FOR SAVING DATA INTO DATABASE
    @PostMapping(value = "/upload")
    @ResponseBody
    @Transactional
    public String save(@RequestParam("username") String username, @RequestParam("email") String email,
            @RequestParam("location") String location,@RequestParam("phone") String phone,
            @RequestParam("imagename") MultipartFile imagename) {
    	StringBuilder builder = new StringBuilder();
        Path fileNameAndPath = Paths.get(uploadDirectory, imagename.getOriginalFilename());
        builder.append(imagename.getOriginalFilename());
        try {
            Files.write(fileNameAndPath, imagename.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
        String image = imagename.getOriginalFilename().toString();
        repository.insertByUserAndImage(username, email, location, phone, image);
        return "success";
    }

    
    //FOR FETCHING ALL JSON DATA
    @GetMapping(value = "/json")
    @ResponseBody
    public List<DatabaseFile> json() {
        return repository.findAll();
    }
}
